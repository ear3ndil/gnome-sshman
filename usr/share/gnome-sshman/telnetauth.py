#!/usr/bin/env python
# -*- coding: UTF8 -*-

# Jordi Ivars Oller
# 2005
# Licensed under GPL <http://www.gnu.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import pexpect,sys,os,gtk,gtk.glade
from os.path import exists

#i18n support
#Try to import locale, if exists

try:

	import locale, gettext
	
	APP='gnome-sshman'
	DIR='/usr/share/locale/'

	#For glade
	locale.setlocale (locale.LC_ALL, '')
	gettext.bindtextdomain (APP, DIR)
	gettext.textdomain (APP)
	gettext.install (APP, DIR, unicode=1)
	gtk.glade.bindtextdomain (APP, DIR)
	gtk.glade.textdomain (APP)
	
	#For python
	t = gettext.translation(APP,DIR)
	_ = t.ugettext
	
	
except:
	
	pass

sys.path.append('/usr/share/gnome-sshman')
import gssh_globals as gglobals
glade_dir=gglobals.glade_dir

class Widgets:

	def __init__(self,file):
		self.widgets = gtk.glade.XML(file)
	def __getitem__(self,key):
		return self.widgets.get_widget(key)


#Get pass from pass_file

def readpass():
		
	home=os.path.expanduser("~")
	pass_file=home+"/.gnome-sshman/.pass"
	
	#If not pass file, exit session
	if not exists (pass_file):
		sys.exit()
		
	f=open(pass_file,"rw")
	a=f.read()
	f.close
	
#Remove pass file and get data, host, passwd, keyringpass and sessionname

	os.system("clear;rm "+pass_file+";clear")
	host=a.split('#')[0]
	passw=a.split('#')[1]
	keypass=a.split('#')[2]
	session=a.split('#')[3]
	port=a.split('#')[4]
	
#Open ssh session with expect
	
	user=host.split("@")[0]
	host=host.split("@")[1]
	
	#print user
	#print host
	#go = pexpect.spawn("telnet "+host)
	go = pexpect.spawn("telnet -l "+user+" "+host +" "+port, timeout=60)
	#go.delaybeforesend = 0
	

	#First ssh return check. authenticity for first ssh session
	#go.expect('ogin: ');
	#go.sendline(user);

	#go.expect('assword:');
	#go.sendline('passw');
	
	
	a=go.expect(['assword'])
	continuar = "si"
	
	
	if a==0:
		
		#go.sendline(user)
		#go.expect(['assword'])
		go.sendline(passw)
		#go.interact()
		
		
	else:
		go.expect (pexpect.EOF)
		go.interact()
		continuar = "no"
		
			
	

#Send password. We can get 2 responses, another password: if password is wrong
#or shell prompt. If get another password then password wrong and open password fialog
#If password is ok, opens session


	print continuar
	if continuar !=  "no":
		i=go.expect(['[#\$] ','.*ogin:'])

		print "i es"
		print i
		if i==10:

			print "--Wrong password--"	
			dialog(host,passw,go,keypass,session)
			gtk.main()
			user=host.split("@")[0]
			go.expect(['assword'])
			go.sendline(passw)
			go.interact()

		if i == 1:
			print "aaa"
			go.interact()

#If session send any error, do a sys.exit

		#try:
		
			#go.interact()
	#	except:
	#		sys.exit()

		sys.exit()
		
	print "An error has ocurred. Press ctrl+D to exit."
#Password dialog

class dialog:


	def __init__(self,server,passw,go,keypass,session):

		askpass_widgets = Widgets(glade_dir+'auth-dialog.glade')
		
		askpass_widgets['authdialog'].connect("destroy", self.on_cancelbutton1_clicked)
		askpass_widgets['authdialog'].connect("key_press_event", self.on_authdialog_key_press_event)	
		
		connections = {
			'okbutton1/clicked' : self.on_okbutton1_clicked,
			'cancelbutton1/clicked' : self.on_cancelbutton1_clicked,
			'entry1/changed'  :  self.on_entry1_changed,
			'checkbutton3/toggled' : self.on_checkbutton3_toggled
				}
	
					
		for wid_con, func in connections.iteritems():
			wid,con = wid_con.split('/')
			askpass_widgets[wid].connect(con,func)


		self.passwin=askpass_widgets['authdialog']
		self.checkbutton3=askpass_widgets['checkbutton3']
		self.entry_passwd=askpass_widgets['entry1']
		self.vbox=askpass_widgets['vbox3']
		self.okbutton1=askpass_widgets['okbutton1']
		
#Pass values to self

		self.go=go
		self.keypass=keypass
		self.session_name=session
		self.server=server

#First send password try is 0

		self.tries=0

#Look if exists a key file for this session_name in send_pass

		home=os.path.expanduser("~")
		self.files=home+"/.gnome-sshman/"
		self.key_file=self.files+session+".key"
		self.session_file=self.files+session+".ssh"
		self.master_key_file=home+"/.gnome-sshman/"+"Master.key"
		
		
		#By default, ok button deactivated
		self.okbutton1.set_sensitive(gtk.FALSE)
		
		self.save_passwd="no"
		if exists (self.session_file) and self.keypass !="no":
			self.checkbutton3.set_sensitive(gtk.TRUE)
						
		if not exists (self.master_key_file):
			self.checkbutton3.set_sensitive(gtk.FALSE)	

#Set blank password by default
		self.password=""
#Capture press return
	def on_authdialog_key_press_event(self,widget,event):
		keyname = gtk.gdk.keyval_name(event.keyval)
		if keyname == "Return" and self.password != "":

###
#TODO: When press return, ok button is pressed
###
			
			self.okbutton1.set_sensitive(gtk.FALSE)
			self.on_okbutton1_clicked(self,widget, event)

	def on_cancelbutton1_clicked(self,widget, *args):
			
		#self.passwin.destroy()
		#Exit session when press cancel button
		#sys.exit()
		self.go.kill(1)
		gtk.main_quit()
		
	def on_okbutton1_clicked(self,widget,*args):
		
		self.passwin.hide()
		self.tries=self.tries+1		
		self.send_pass(self.server,self.password)		
			
		
	def on_checkbutton3_toggled(self,widget, *args):

#Save password
		if self.checkbutton3.get_active():
			
			self.save_passwd="yes"
		else:
		
			self.save_passwd="no"
		

	def on_entry1_changed(self,widget,*args):
				
		self.password=self.entry_passwd.get_text()
		
		if self.password != "":
			self.okbutton1.set_sensitive(gtk.TRUE)
		elif self.password == "":
			self.okbutton1.set_sensitive(gtk.FALSE)		
	
	
	def send_pass(self,host,passw):

#If save_password is yes, open session_name file. If no keyring opened (in pass_file, 
#keyringpass=no) asks for keyring dialog, if is opened, use it.

		if self.save_passwd=="yes":
				
			file_opened=self.files+self.session_name
			a=open(file_opened, 'w')
			a.write(self.password)
			a.close()
			
			if self.keypass !="no":
						
				file=file_opened
				key=self.keypass
					
				import cipher
				cipher.Encipher(file,key)
				os.system("rm "+file_opened)
				
				self.passwin.hide()
				self.go.sendline(self.password)
	
				i=self.go.expect(['.*ssword','[#\$] '])
				
				if i==0:
				
		#On ok button. If try es < 2, try with another password. If not, kill session and exits.					
		#2 passwords tries + first try=3 tries, maximum for ssh server
		
					print "--Wrong password--"	
					if self.tries == 2:
						self.go.kill(1)
						gtk.main_quit()
					else:
						self.entry_passwd.set_text("")
						self.passwin.show()
							
				elif i==1: 
					
					gtk.main_quit()
					print "--Connection established--"
					self.go.send("\n")
								
			else:
				self.passwin.hide()
				self.askpassring()

		
		else:
		
			
			self.go.sendline(self.password)
	
			i=self.go.expect(['.*ssword','[#\$] '])
				
			if i==0:
									
				print "--Wrong password--"	
				
				if self.tries == 2:
					self.go.kill(1)
					gtk.main_quit()
				else:
					self.entry_passwd.set_text("")
					self.passwin.show()
				
			elif i==1: 
				
				gtk.main_quit()
				print "--Connection established--"
				self.go.send("\n")
				
					
#Open keyring dialog
	def askpassring(self):

		
		keyring_widgets = Widgets(glade_dir+'keyring.glade')
		keyring_widgets['keyring'].connect("destroy", self.on_button1_clicked)
		keyring_widgets['keyring'].connect("key_press_event", self.on_keyring_press_event)
		
		connections = {
			'button2/clicked' : self.on_button2_clicked,  
			'button1/clicked' : self.on_button1_clicked,  
			'entry_keyring/changed'  : self.on_entry_keyring_changed
			}
	
					
		for wid_con, func in connections.iteritems():
			wid,con = wid_con.split('/')
			keyring_widgets[wid].connect(con,func)
			
						
		self.keyringwin=keyring_widgets['keyring']
		self.key_password=keyring_widgets['entry_keyring']
		self.okbutton2=keyring_widgets['button2']
		
		#By default, ok button deactivated
		self.okbutton2.set_sensitive(gtk.FALSE)
		
		#To control max tryes for keyring
		self.exec_times = 0

#Set keyring password to blank
		self.keyring_ask = ""
		
#Capture return key		
	def on_keyring_press_event(self,widget,event):
		keyname = gtk.gdk.keyval_name(event.keyval)
		#print "Key %s (%d) was pressed" % (keyname, event.keyval)
		if keyname == "Return" and self.keyring_ask != "":
			self.on_button2_clicked(self,widget, event)

	def on_button1_clicked(self,widget, *args):
	
	#If cancel button pressed in keyring, continue
		self.keypass="no"
		self.save_passwd="no"
		self.send_pass(self.server,self.password)
		self.keyringwin.destroy()
		#self.tries=0
		self.checkbutton3.set_sensitive(gtk.FALSE)
					
	def on_entry_keyring_changed(self,widget, *args):
		
		self.keyring_ask=self.key_password.get_text()
		
		if self.keyring_ask != "":
			self.okbutton2.set_sensitive(gtk.TRUE)
		elif self.keyring_ask == "":
			self.okbutton2.set_sensitive(gtk.FALSE)	
			
	def on_button2_clicked(self,widget, *args):
		
		self.exec_times = self.exec_times+1
		home=os.path.expanduser("~")
		self.keys=home+"/.gnome-sshman/"	
		self.master_key_file=self.keys+"Master.key"
				
		file=self.master_key_file
		key=self.keyring_ask
	
#Decrypt master file, try to open it and remove
		
		import cipher
		cipher.Decipher(file,key)
				
		file_opened=self.keys+"Master"
		self.keyringwin.hide()
#If can not open keyring, open password dialog withouth save passwd option active
#send keypass as "no"
		
		try:
			
			o=open(file_opened, "r")
			self.keypass=key			
			o.close()		
			os.system("rm "+file_opened)
			self.send_pass(self.server,self.password)
			#self.keyringwin.destroy()
			
		except:
						
			if self.exec_times == 3:
				self.keypass="no"
				self.save_passwd="no"
				self.send_pass(self.server,self.password)
				self.keyringwin.destroy()
				self.tries=0
				self.checkbutton3.set_sensitive(gtk.FALSE)
				#self.passwin.show()	
			else:
				
				self.key_password.set_text("")
				#self.keyringwin.hide()
				self.keyringwin.show()
				
		

		
readpass()
