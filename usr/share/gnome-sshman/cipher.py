#!/usr/bin/env python 
# -*-Python-*-
# Cipher 1.00
#
# Part of the Python Cryptography Toolkit
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
#
# Modified by Jordi Ivars and relicensed under GPL v2
#                                                                                 
# This program is free software; you can redistribute it and/or               
# modify it under the terms of the GNU General Public License                 
# as published by the Free Software Foundation; either version 2              
# of the License, or (at your option) any later version.                      
#                                                                                     
# This program is distributed in the hope that it will be useful,             
# but WITHOUT ANY WARRANTY; without even the implied warranty of              
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               
# GNU General Public License for more details.                                
#                                                                                     
# You should have received a copy of the GNU General Public License           
# along with this program; if not, write to the Free Software                 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. 
#####################################################################


import sys, getopt, os

executable = os.path.basename(sys.argv[0])
if executable=='': executable='cipher'
NoInputFile = ''                        
NoOutputFile = ''


def GenerateIV(length):
    import random
    IV=''
    for i in range(0, length):
        IV=IV + chr(int(256*random.random()))
    return IV
    
def Encipher(filename, key):

	a=0
	for x in key:
		a=a+1
			
	key=(a, key)

	magic = 'ctx\001'  
	NoInputFile = ''                        
	NoOutputFile = ''
	
	cipher = "AES"
	if (cipher==''): cipher='IDEA'
	try:
		exec ('from Crypto.Cipher import '+cipher)
		module=eval(cipher)
	except ImportError:
		print executable+ ':', cipher, ': Cipher does not exist.'
		sys.exit(1)
	import Crypto.Hash.MD5
	try:
		input=open(filename, 'r')
	except IOError:
		raise NoInputFile
	try:
		output=open(filename+".key", 'w')
	except IOError:
		raise NoOutputFile, filename+'.key'

	if (key[0]==0):
		key=raw_input(' ')
	else: key=key[1]
	key=Crypto.Hash.MD5.new(key).digest()
	IV=''
	for i in range(0, module.block_size): IV=IV+'A'
	if (module.key_size==0):
		cipherobj=module.new(key, module.MODE_CBC, IV)
	else:
		#cipherobj=module.new(key[0:module.key_size], module.MODE_CBC, IV)
		cipherobj=module.new(key, module.MODE_CBC, IV)
	output.write(magic+cipher+'\0')
	data = GenerateIV(module.block_size)
	filedata=input.read()
	data = data + magic + str(len(filedata))+'\0'+filename+'\0'
	data = data + filedata
	input.close()
	padding=module.block_size - (len(data) % module.block_size)
	for i in range(0, padding):
		data = data + chr(i)
	ciphertext=cipherobj.encrypt(data)
	output.write(ciphertext)
	output.close()

def Decipher(filename, key):

	a=0
	for x in key:
		a=a+1
			
	key=(a, key)
	cipher="AES"
	magic = 'ctx\001'
	NoInputFile = ''                        
	NoOutputFile = ''
	import Crypto.Hash.MD5, string
	try:
		input=open(filename, 'r')
	except IOError:
		raise NoInputFile
	if (input.read(len(magic))!=magic):
		
		return
	t=''
	while (1):
		c=input.read(1)
		if (ord(c)==0): break
		t=t+c
	if (cipher==''): cipher=t
	try:
		from Crypto.Cipher import *
		module=eval(cipher)
	except ImportError:
		
		sys.exit(1)
	if (key[0]==0):
		key=raw_input(' ')
	else: key=key[1]
	key=Crypto.Hash.MD5.new(key).digest()
	IV = ''
	for i in range(0, module.block_size): IV=IV+'A'
	data=input.read()
	if (module.key_size==0):
		cipherobj=module.new(key, module.MODE_CBC, IV)
	else:
		#cipherobj=module.new(key[0:module.key_size], module.MODE_CBC,IV)
		cipherobj=module.new(key, module.MODE_CBC, IV)
	plain=cipherobj.decrypt(data)       # Decrypt the data
	plain=plain[module.block_size:]      # Discard first block of random data
	if (plain[0:len(magic)]!=magic):
		
		return
	else: plain=plain[len(magic):]
	i=string.find(plain, '\0')
	length=string.atoi(plain[0:i])
	j=string.find(plain, '\0', i+1)
	newfilename=plain[i+1:j]
	try:
		output=open(newfilename, 'w')
	except IOError:
		raise NoOutputFile, newfilename
	output.write(plain[j+1:j+1+length])
	output.close()
